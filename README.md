# Simple moddable example Godot project - mods

A couple of example mods for the [simple moddable example Godot project](https://gitlab.com/Zatherz/godot-simple-moddable-example).

Each mod is a directory containing the files `icon.png`, `label.tscn` and `engine.cfg`.
Only `label.tscn` matters, the rest is just for making it possible to edit the mods in the Godot editor.

When the project is ran, each `label.tscn` is loaded, then instanced and added to the VBoxContainer.

### Installing

On Linux, put this in `$HOME/.godot/app_userdata/godot-simple-moddable-example`.
